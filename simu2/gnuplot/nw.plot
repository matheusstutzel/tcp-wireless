reset
set encoding utf8
fontsize = 18
set term png
set output "nw.png"
set style fill solid 1.00 border 0
set style histogram errorbars gap 2 lw 1
set style data histograms
set xtics rotate by -45
#set xlabel "Probabilidade de perda de pacotes"
#set ylabel "Vazão (Mbps)"
set bars 0.6
set yrange [0 : 40]
set datafile separator ","
plot 'nw.dat' \
		   using 2:3:4:xtic(1) ti "New Reno" linecolor rgb "#FF0000", \
        '' using 5:6:7 ti "Westwood+" lt 1 lc rgb "#00FF00", \