/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/netanim-module.h"

#include <iostream>
#include <fstream>
#include <string>
#ifndef DEBUG
#define DEBUG true
#endif

using namespace ns3;


NodeContainer nodes;
/*
	A1 				   A2==AUXA
	   \			  /
	    \			 /
	     AP1 ==== AP2
	    /			 \
	   /			  \
	B1				   B2==AUXB

*/


std::ofstream myfile;


Ptr<PacketSink> A2,B2; 
double vazao,v;
uint64_t ultimoRX=0,u=0,P1=0,P2=0,I1=0,I2=0;
void calcVazao(){
	vazao = (A2->GetTotalRx () - ultimoRX) *  8.0 / 1e6;     
	v = (B2->GetTotalRx () - u) *  8.0 / 1e6;     
	ultimoRX = A2->GetTotalRx ();
	u = B2->GetTotalRx () ;
  	std::cout <<vazao<<" "<<v<< std::endl;
  	myfile <<vazao<< std::endl;
	Simulator::Schedule (MilliSeconds (1000), &calcVazao);
}


void calcP1(){
	P1 = (A2->GetTotalRx () );
	I1 = (B2->GetTotalRx () );
}


void calcP2(){
	P2 = (A2->GetTotalRx () );
	I2 = (B2->GetTotalRx () );
}

int main(int argc, char *argv[]){
	std::string tcpVariantB =  "TcpWestwoodPlus";
	std::string tcpVariantA =  "TcpNewReno";
	int round = 1;
	double total = 180.0;

	double error = 10e-4;	


	CommandLine cmd;
	cmd.AddValue ("tcpVariantA", "Transport protocol to use: TcpNewReno, TcpVegas, TcpWestwoodPlus ", tcpVariantA);
	cmd.AddValue ("tcpVariantB", "Transport protocol to use: TcpNewReno, TcpVegas, TcpWestwoodPlus ", tcpVariantB);
  	cmd.AddValue ("round","Round",round);
  	cmd.Parse (argc, argv);

  	if(tcpVariantA.compare(tcpVariantB)==0){
  		std::cout<<"As versões do TCP devem ser diferentes!!"<<std::endl;
  		exit(-1);
  	}


	std::string filename="Resultado2_"+tcpVariantA+"_"+tcpVariantB+"_"+std::to_string(error)+"_"+std::to_string(total)+"_"+std::to_string(round);
	std::cout<<filename<<std::endl;
	myfile.open (filename);
 	tcpVariantA = std::string ("ns3::") + tcpVariantA;
 	tcpVariantB = std::string ("ns3::") + tcpVariantB;

	RngSeedManager::SetSeed (2017);  
	RngSeedManager::SetRun (round); 


	NodeContainer ap_0;
	ap_0.Create (1);
	NodeContainer ap_1;
	ap_1.Create (1);
	NodeContainer station_0;
	station_0.Create (1);
	NodeContainer station_1;
	station_1.Create (1);
	NodeContainer station_2;
	station_2.Create (1);
	NodeContainer station_3;
	station_3.Create (1);
	NodeContainer auxa;
	auxa.Create (1);
	NodeContainer auxb;
	auxb.Create (1);


        
	//Criando nós aux para inserir erro na rede
	PointToPointHelper pointToPointa;
	pointToPointa.SetDeviceAttribute ("DataRate", StringValue ("1000Mbps"));
	pointToPointa.SetChannelAttribute ("Delay", StringValue ("0ms"));
	
	NodeContainer all_p2p_2;
	all_p2p_2.Add (station_2);
	all_p2p_2.Add (auxa);

	NetDeviceContainer ap2p = pointToPointa.Install (all_p2p_2);

    PointToPointHelper pointToPointb;
	pointToPointb.SetDeviceAttribute ("DataRate", StringValue ("1000Mbps"));
	pointToPointb.SetChannelAttribute ("Delay", StringValue ("0ms"));
    
    NodeContainer all_p2p_3;
	all_p2p_3.Add (station_3);
	all_p2p_3.Add (auxb);

    NetDeviceContainer bp2p = pointToPointb.Install (all_p2p_3);
        
    PointToPointHelper pointToPointn;
	pointToPointn.SetDeviceAttribute ("DataRate", StringValue ("1000Mbps"));
	pointToPointn.SetChannelAttribute ("Delay", StringValue ("0ms"));

	NodeContainer all_p2p_1;
	all_p2p_1.Add (ap_0);
	all_p2p_1.Add (ap_1);
	NetDeviceContainer np2p = pointToPointn.Install (all_p2p_1);


	Ptr<RateErrorModel> em = CreateObject<RateErrorModel> ();
    em->SetAttribute ("ErrorRate", DoubleValue (error));
    em->SetAttribute ("ErrorUnit",StringValue("ERROR_UNIT_PACKET") );
    ap2p.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em));
    bp2p.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em));


    //WIFI AP1
	YansWifiChannelHelper channel1 = YansWifiChannelHelper::Default ();
	channel1.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
	YansWifiPhyHelper phy1 = YansWifiPhyHelper::Default ();
	phy1.SetChannel (channel1.Create ());
	WifiMacHelper mac1;
	WifiHelper wifi1;
	wifi1.SetStandard (WIFI_PHY_STANDARD_80211n_5GHZ);
	wifi1.SetRemoteStationManager ("ns3::IdealWifiManager");
	NetDeviceContainer ap1;
	mac1.SetType ("ns3::StaWifiMac");
	ap1 = (wifi1.Install (phy1, mac1, station_0.Get(0)));
	ap1.Add(wifi1.Install (phy1, mac1, station_1.Get(0)));
	mac1.SetType ("ns3::ApWifiMac");
	ap1.Add(wifi1.Install (phy1, mac1, ap_0.Get(0)));

	//WIFI AP2
	YansWifiChannelHelper channel2 = YansWifiChannelHelper::Default ();
	channel2.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
	YansWifiPhyHelper phy2 = YansWifiPhyHelper::Default ();
	phy2.SetChannel (channel2.Create ());
	WifiMacHelper mac2;
	WifiHelper wifi2;
	wifi2.SetStandard (WIFI_PHY_STANDARD_80211n_5GHZ);
	wifi2.SetRemoteStationManager ("ns3::IdealWifiManager");
	NetDeviceContainer ap2;
	mac2.SetType ("ns3::StaWifiMac");
	ap2.Add(wifi2.Install (phy2, mac2, station_2.Get(0)));
	ap2.Add(wifi2.Install (phy2, mac2, station_3.Get(0)));
	mac2.SetType ("ns3::ApWifiMac");
	ap2.Add(wifi2.Install (phy2, mac2, ap_1.Get(0)));


	MobilityHelper mobility;
	Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
	positionAlloc->Add (Vector (0.0, 0.1, 0.1));
	positionAlloc->Add (Vector (0.0, 0.2, 0.1));
	positionAlloc->Add (Vector (0.0, 0.0, 0.0));
	positionAlloc->Add (Vector (0.0, 0.0, 0.2));
	positionAlloc->Add (Vector (0.0, 0.3, 0.0));
	positionAlloc->Add (Vector (0.0, 0.3, 0.2));
	positionAlloc->Add (Vector (0.0, 0.4, 0.0));
	positionAlloc->Add (Vector (0.0, 0.4, 0.2));
	mobility.SetPositionAllocator (positionAlloc);
	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mobility.Install (ap_0);
	mobility.Install (ap_1);
	mobility.Install (station_0);
	mobility.Install (station_1);
	mobility.Install (station_2);
	mobility.Install (station_3);
	mobility.Install (auxa);
	mobility.Install (auxb);

	InternetStackHelper stack;
	stack.Install (ap_0);
	stack.Install (ap_1);
    


    if (tcpVariantA.compare ("ns3::TcpWestwoodPlus") == 0){ 
		Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (TcpWestwood::GetTypeId ()));
		Config::SetDefault ("ns3::TcpWestwood::ProtocolType", EnumValue (TcpWestwood::WESTWOODPLUS));
	}else{
		TypeId tcpTid;
		NS_ABORT_MSG_UNLESS (TypeId::LookupByNameFailSafe (tcpVariantA, &tcpTid), "TypeId " << tcpVariantA << " not found");
		Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (TypeId::LookupByName (tcpVariantA)));
	}
	
	InternetStackHelper stackA;
	stackA.Install (station_0);
	stackA.Install (station_2);
	stackA.Install (auxa);

    if (tcpVariantB.compare ("ns3::TcpWestwoodPlus") == 0){ 
		Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (TcpWestwood::GetTypeId ()));
		Config::SetDefault ("ns3::TcpWestwood::ProtocolType", EnumValue (TcpWestwood::WESTWOODPLUS));
	}else{
		TypeId tcpTid;
		NS_ABORT_MSG_UNLESS (TypeId::LookupByNameFailSafe (tcpVariantB, &tcpTid), "TypeId " << tcpVariantB << " not found");
		Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (TypeId::LookupByName (tcpVariantB)));
	}


	InternetStackHelper stackB;
	stackB.Install (station_1);
	stackB.Install (station_3);
	stackB.Install (auxb);


	

	Ipv4AddressHelper address;
	address.SetBase ("192.168.1.0", "255.255.255.0");
	Ipv4InterfaceContainer iap1 = address.Assign (ap1);

	address.SetBase ("192.168.2.0", "255.255.255.0");
	Ipv4InterfaceContainer iap2 = address.Assign (ap2);

	address.SetBase("192.168.3.0","255.255.255.0");
	Ipv4InterfaceContainer inn = address.Assign (np2p);
	
	
	address.SetBase("192.168.4.0","255.255.255.0");
	Ipv4InterfaceContainer ia = address.Assign (ap2p);
	
	address.SetBase("192.168.5.0","255.255.255.0");
	Ipv4InterfaceContainer ib = address.Assign (bp2p);

	Ipv4GlobalRoutingHelper::PopulateRoutingTables ();




	uint16_t port = 8080; 


	//Configurando a conexão entre A1 e AUXA(A2)
	BulkSendHelper source ("ns3::TcpSocketFactory",InetSocketAddress (ia.GetAddress (1), port));
	source.SetAttribute ("MaxBytes", UintegerValue (0));	
	ApplicationContainer sourceApps = source.Install (station_0.Get (0));
	sourceApps.Start (Seconds (1.0));
	sourceApps.Stop (Seconds (total));
	PacketSinkHelper sink ("ns3::TcpSocketFactory",InetSocketAddress (Ipv4Address::GetAny (), port));
	ApplicationContainer sinkApps = sink.Install (auxa.Get (0));
	sinkApps.Start (Seconds (0.0));
	sinkApps.Stop (Seconds (total));
	A2 = StaticCast<PacketSink> (sinkApps.Get (0));

	//Configurando a conexão entre B1 e AUXB(B2)
	BulkSendHelper inter ("ns3::TcpSocketFactory",InetSocketAddress (ib.GetAddress (1), port));
	inter.SetAttribute ("MaxBytes", UintegerValue (0));	
	ApplicationContainer interApp = inter.Install (station_1.Get (0));
	interApp.Start (Seconds (1.0+(total/3)));
	interApp.Stop (Seconds (1+2*(total/3)));
	PacketSinkHelper sink2 ("ns3::TcpSocketFactory",InetSocketAddress (Ipv4Address::GetAny (), port));
	ApplicationContainer sinkApps2 = sink2.Install (auxb.Get (0));
	sinkApps2.Start (Seconds (0.0));
	sinkApps2.Stop (Seconds (total));
	B2 = StaticCast<PacketSink> (sinkApps2.Get (0));




	Simulator::Schedule (Seconds (2), &calcVazao);
	Simulator::Schedule (Seconds (1.0+(total/3)), &calcP1);
	Simulator::Schedule (Seconds (1.0+2*(total/3)), &calcP2);


	Simulator::Stop (Seconds (total + 1));
  	Simulator::Run ();
  	Simulator::Destroy ();
	
	double p = (total/3);
	double M1,M2,M3;	
	M1 = (((P1) * 8) / (1e6  * (p)));
	M2 = (((P2-P1) * 8) / (1e6  * (p)));
	M3 = (((A2->GetTotalRx () - P2) * 8) / (1e6  * (p-1)));

	//salvando M1 M2 M3
	myfile <<M1<<" "<<M2<<" "<<M3<< std::endl;


	M1 = (((I1) * 8) / (1e6  * (p)));
	M2 = (((I2-I1) * 8) / (1e6  * (p)));
	M3 = (((B2->GetTotalRx () - I2) * 8) / (1e6  * (p)));

	//salvando M1 M2 M3
	myfile <<M1<<" "<<M2<<" "<<M3<< std::endl;
	myfile.close();
	return 0;
}
