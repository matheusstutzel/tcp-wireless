import numpy as np
import scipy as sp
import scipy.stats
import os
import sys
import os.path

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m, m-h, m+h

def tail(f, n):
	stdin,stdout = os.popen2("tail -n "+str(n)+" "+f.name)
	stdin.close()
	lines = stdout.readlines(); stdout.close()
	resposta = []
	for i in range(0,n):
		r = lines[i].replace('\n', '').split(" ")
		for w in r:
			resposta.append(w)
	return resposta

t = ["WestwoodPlus","NewReno","Vegas"]

etapas=["A1-T0","A1-T1","A1-T2","B1-T0","B1-T1","B1-T2"]
for tcp in t:
	for tcp2 in t:
		if tcp2==tcp:
			continue
		dados = []
		for i in range(0,6):
			dados.append([])

		for i in range(1,50):
			filename ="Resultado2/Resultado2_Tcp{0:s}_Tcp{1:s}_0.001000_180.000000_{2:d}".format(tcp,tcp2,i)
			if not os.path.exists(filename):
				continue
			rodada = (tail(open(filename, "r"),2))
			for i in range(0,6):
				dados[i].append(float(rodada[i]))
		for i in range(0,6):
			nome =  tcp+"-"+tcp2 +"-"+etapas[i]
			a,b,c = mean_confidence_interval(dados[i],0.99)
			sys.stdout.write("{}, {}, {}, {}".format(nome,a,b,c))
			print("")

f.close()


