/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/netanim-module.h"

#include <iostream>
#include <fstream>
#include <string>
#ifndef DEBUG
#define DEBUG true
#endif

using namespace ns3;


NodeContainer nodes;
/*
	A1 --- AP1 --- B1==AUX
*/


std::ofstream myfile;


Ptr<PacketSink> B1; 
double vazao;
uint64_t ultimoRX=0;
void calcVazao(){
	vazao = (B1->GetTotalRx () - ultimoRX) *  8.0 / 1e6;     
	ultimoRX = B1->GetTotalRx ();
  	std::cout <<vazao<< std::endl;
  	myfile <<vazao<< std::endl;
	Simulator::Schedule (MilliSeconds (1000), &calcVazao);
}

int main(int argc, char *argv[]){
	std::string tcpVariant =  "TcpWestwoodPlus";
	
	double error = 10e-6;	
	double total = 60.0;
	int round = 1;


	CommandLine cmd;
	cmd.AddValue ("error", "Error", error);
	cmd.AddValue ("time", "Duração do teste", total);
	cmd.AddValue ("tcpVariant", "Transport protocol to use: TcpNewReno, TcpVegas, TcpWestwoodPlus ", tcpVariant);
  	cmd.AddValue ("round","Round",round);
  	cmd.Parse (argc, argv);


	RngSeedManager::SetSeed (2017);  
	RngSeedManager::SetRun (round); 

	std::string filename="Resultado_"+tcpVariant+"_"+std::to_string(error)+"_"+std::to_string(total)+"_"+std::to_string(round);
	std::cout<<filename<<std::endl;
	myfile.open (filename);

	tcpVariant = std::string ("ns3::") + tcpVariant;



	//aux contem [B1,aux]
	//nodes contem [A1,AP1,B1]
	NodeContainer nodes,aux;
	aux.Create(2);
	nodes.Create (2);
	nodes.Add(aux.Get(0));
	

	//Criando nó aux para inserir erro na rede
	PointToPointHelper pointToPoint;
	pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("1000Mbps"));
	pointToPoint.SetChannelAttribute ("Delay", StringValue ("0ms"));
	NetDeviceContainer p2pDevices = pointToPoint.Install (aux);


	Ptr<RateErrorModel> em = CreateObject<RateErrorModel> ();
    em->SetAttribute ("ErrorRate", DoubleValue (error));
    em->SetAttribute ("ErrorUnit",StringValue("ERROR_UNIT_PACKET") );
    p2pDevices.Get (1)->SetAttribute ("ReceiveErrorModel", PointerValue (em));


	YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
	channel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");

	YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();

	phy.SetChannel (channel.Create ());
	

	WifiMacHelper mac;

	WifiHelper wifi;
	wifi.SetStandard (WIFI_PHY_STANDARD_80211n_5GHZ);
	wifi.SetRemoteStationManager ("ns3::IdealWifiManager");

	
	NetDeviceContainer devices;

	mac.SetType ("ns3::StaWifiMac");
	devices=(wifi.Install (phy, mac, nodes.Get(0)));
	devices.Add(wifi.Install (phy, mac, nodes.Get(1)));
	mac.SetType ("ns3::ApWifiMac");
	devices.Add(wifi.Install (phy, mac, nodes.Get(2)));


	if(DEBUG)std::cout<<"Devices = "<<devices.GetN()<<"\n";

	if (tcpVariant.compare ("ns3::TcpWestwoodPlus") == 0){ 
		Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (TcpWestwood::GetTypeId ()));
		Config::SetDefault ("ns3::TcpWestwood::ProtocolType", EnumValue (TcpWestwood::WESTWOODPLUS));
	}else{
		TypeId tcpTid;
		NS_ABORT_MSG_UNLESS (TypeId::LookupByNameFailSafe (tcpVariant, &tcpTid), "TypeId " << tcpVariant << " not found");
		Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (TypeId::LookupByName (tcpVariant)));
	}

	InternetStackHelper stack;
	stack.Install (nodes);
	stack.Install (aux.Get(1));

	Ipv4AddressHelper address;
	address.SetBase ("192.168.1.0", "255.255.255.0");
	Ipv4InterfaceContainer interfaces = address.Assign (devices);

	address.SetBase("192.168.2.0","255.255.255.0");
	Ipv4InterfaceContainer p2pinterfaces = address.Assign (p2pDevices);

	Ipv4GlobalRoutingHelper::PopulateRoutingTables ();


	
	MobilityHelper mobility;
	Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
	positionAlloc->Add (Vector (0.0, 0.0, 0.0));
	positionAlloc->Add (Vector (0.1, 0.0, 0.0));
	positionAlloc->Add (Vector (0.2, 0.0, 0.0));
	mobility.SetPositionAllocator (positionAlloc);
	mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mobility.Install (nodes);

	uint16_t port = 8080; 

	BulkSendHelper source ("ns3::TcpSocketFactory",InetSocketAddress (p2pinterfaces.GetAddress (1), port));
	source.SetAttribute ("MaxBytes", UintegerValue (0));
	
	ApplicationContainer sourceApps = source.Install (nodes.Get (0));
	sourceApps.Start (Seconds (1.0));
	sourceApps.Stop (Seconds (total));

	PacketSinkHelper sink ("ns3::TcpSocketFactory",InetSocketAddress (Ipv4Address::GetAny (), port));

	ApplicationContainer sinkApps = sink.Install (aux.Get (1));
	sinkApps.Start (Seconds (0.0));
	sinkApps.Stop (Seconds (total));

	B1 = StaticCast<PacketSink> (sinkApps.Get (0));

	if(DEBUG)Simulator::Schedule (Seconds (2), &calcVazao);


	Simulator::Stop (Seconds (total + 1));
  	Simulator::Run ();
  	Simulator::Destroy ();
	
	myfile <<((B1->GetTotalRx () * 8) / (1e6  * (total-1)))<< std::endl;
	myfile.close();
	return 0;
}
