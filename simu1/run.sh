for i in `seq 1 100`;
        do
./waf --run "scratch/simu --time=60 --error=1e-4 --tcpVariant=TcpWestwoodPlus --round=$i"
./waf --run "scratch/simu --time=60 --error=1e-4 --tcpVariant=TcpNewReno --round=$i"
./waf --run "scratch/simu --time=60 --error=1e-4 --tcpVariant=TcpVegas --round=$i"

./waf --run "scratch/simu --time=60 --error=1e-3 --tcpVariant=TcpWestwoodPlus --round=$i"
./waf --run "scratch/simu --time=60 --error=1e-3 --tcpVariant=TcpNewReno --round=$i"
./waf --run "scratch/simu --time=60 --error=1e-3 --tcpVariant=TcpVegas --round=$i"

./waf --run "scratch/simu --time=60 --error=1e-2 --tcpVariant=TcpWestwoodPlus --round=$i"
./waf --run "scratch/simu --time=60 --error=1e-2 --tcpVariant=TcpNewReno --round=$i"
./waf --run "scratch/simu --time=60 --error=1e-2 --tcpVariant=TcpVegas --round=$i"

done