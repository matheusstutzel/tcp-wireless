reset
set encoding utf8
fontsize = 18
set term png 
set output "simu1.png"
set boxwidth 0.9
set style fill solid 1.00 border 0
set style histogram errorbars gap 3 lw 1
set style data histograms
set xtics rotate by -45
set xlabel "Probabilidade de perda de pacotes"
set ylabel "Vazão (Mbps)"
set bars 0.6
set yrange [0 : 50]
set datafile separator ","
plot 'resultadosSimu1.dat' \
		   using 2:3:4:xtic(1) ti "Westwood+" linecolor rgb "#FF0000" fs pattern 1, \
        '' using 5:6:7 ti "New Reno" lt 1 lc rgb "#00FF00" fs pattern 2, \
        '' using 8:9:10 ti "Vegas" lt 1 lc rgb "#0000FF" fs pattern 4